# 视频资讯 app

#### 介绍
毕业设计Android 的实现

#### 软件架构
软件架构说明
1、Android Studio 4.1
2、mongdb
3、Mysql 8.0

#### 安装教程

1.  环境搭建：
    （1）、Mysql 8.0
    （2）、Mongdb
    
2.  导入Mysql 文件
    启动Mongdb
3.  启动后台项目【人人网】
4.  即可运行Android Studio项目
5.  注意修改项目中的IP
6、后端接口：http://47.105.65.35:8080/renren-fast

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

#### 项目展示

1、首页展示：

![输入图片说明](https://images.gitee.com/uploads/images/2021/0528/221358_4e55bf14_9031761.jpeg "1.jpg")

2、个人中心

![输入图片说明](https://images.gitee.com/uploads/images/2021/0528/221335_06263ed5_9031761.jpeg "2.jpg")
3、资讯模块

![输入图片说明](https://images.gitee.com/uploads/images/2021/0528/221409_1fdc3112_9031761.jpeg "3.jpg")

4、首页展示二
![输入图片说明](https://images.gitee.com/uploads/images/2021/0528/221418_3939ce33_9031761.jpeg "4.jpg")
#### 特技

5、收藏展示
![输入图片说明](https://images.gitee.com/uploads/images/2021/0616/164650_ad2e99ae_9031761.png "collection.png")

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
